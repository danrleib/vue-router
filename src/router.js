import Vue from 'vue'
import VueRouter from 'vue-router'

import Erro404 from './views/Erro404.vue'
import Erro404Contatos from './views/contatos/Erro404Contatos'
import Login from './views/login/Login'
import EventBus from './event-bus'
import PaginaInicial from './views/PaginaInicial'

const Contato = () => import('./views/contatos/Contato.vue')
const Home = () => import('./views/Home.vue')
const ContatoDetalhes = () => import('./views/contatos/ContatoDetalhes.vue')
const ContatosHome = () => import('./views/contatos/ContatosHome')
const ContatoEditar = () => import('./views/contatos/ContatoEditar')
const ContatoNovo = () => import('./views/contatos/ContatoNovo')

Vue.use(VueRouter)

const extrairParametroId = route => ({
  id: parseInt(route.params.id)

})

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/paginainicial',
      component: PaginaInicial,
      meta: { requerAutenticação: true },

      children: [{
        path: '/contatos',
        component: Contato,
        // props: (route) => {
        //   const busca = route.query.busca
        //   return busca ? { busca } : {}
        // },
        children: [{
          path: ':id',
          component: ContatoDetalhes,
          name: 'contato',
          props: extrairParametroId
        },
        {
          path: '',
          component: ContatosHome
        },
        {
          path: ':id/editar',
          beforeEnter(to, from, next) {
            console.log('beforeEnter')
            next()
            // next('/contatos) redirecionar
            // next(new Error (`premissoes insuficientes "${to.fullPath}""`))

          },
          components: {
            default: ContatoEditar,
            'contato-detalhes': ContatoDetalhes
          },
          props: {
            default: extrairParametroId,
            'contato-detalhes': extrairParametroId
          }
        },
        {
          path: '/new',
          component: ContatoNovo
        },
        {
          path: '*', component: Erro404Contatos
        }]
      },
      { path: '/home', component: Home }],
    },

    {
      path: '/', component: Login,
    },

    {
      path: '*', component: Erro404
    }
  ]
})

router.beforeEach((to, from, next) => {
  console.log('beforeEach')
  console.log('requer autenticação?', to.meta.requerAutenticação)
  const estaAutenticado = EventBus.autenticado
  if (to.matched.some(rota => rota.meta.requerAutenticação)) {
    if (!estaAutenticado) {
      next(
        {
          path: '/',
          query: { redirecionar: to.fullPath }
        }
      )
      return
    }
  }
  next()
})

router.afterEach(() => {
  console.log('afterEach')
})

router.beforeResolve((to, from, next) => {
  console.log('beforeReolve')
  next()
})

// router.onError(erro) => {
//   console.log(erro)
// }
export default router